<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

class UsersController extends AppController
{
    public function beforeFilter(Event $event){
        parent::beforeFilter($event);
        $this->Auth->allow('add');
    }

    public function login()
    {
        if ($this->request->is('post')) {
        $user = $this->Auth->identify();
        if ($user) {
            $this->Auth->setUser($user);
            return $this->redirect($this->Auth->redirectUrl());
        }
        $this->Flash->error('Your email or password is incorrect.');
    }

    }

    public function logout(){
        $this->Flash->success('You are now logged out.');
        return $this->redirect($this->Auth->logout());
    }
    
    public function index()
    {
        $users = $this->paginate($this->Users);
        //$this->set(['user'->$userslist]);
        $this->set(compact('users'));
    }

    public function add()
    {
        $user = $this->Users->newEntity();
        
        if($this->request->is('post')){
            
            $user = $this->Users->patchEntity($user, $this->request->data);
            
            if($this->Users->save($user)){
                $this->Flash->success('Salvo com sucesso');
                $this->redirect(['action' => 'index']);
            }

            else{
                $this->Flash->errorx('Não pode ser salvo');
            }
        }
        $this->set(compact('user'));
    }

    public function edit($id)
    {
        $user = $this->Users->get($id);  
        if($this->request->is(['post', 'put'])){
            $user = $this->Users->patchEntity($user, $this->request->data);
            if($this->Users->save($user)){
                $this->Flash->success('Salvo com sucesso');
                $this->redirect(['action' => 'index']);
            }
            else{
                $this->Flash->error('Não pode ser salvo');
            }
        }
        $this->set(compact('user'));
    }
    public function view($id)
    {
        $user = $this->Users->get($id);
        $this->set(compact('user'));
    }

    public function delete($id)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if($this->Users->delete($user)){
            $this->Flash->success('Removido com sucesso');
        }
        else{
            $this->Flash->error('Não pode ser removido');
        }

        $this->redirect(['action' => 'index']);
    }
}