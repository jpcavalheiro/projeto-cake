/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100410
 Source Host           : localhost:3300
 Source Schema         : curso_cake3

 Target Server Type    : MySQL
 Target Server Version : 100410
 File Encoding         : 65001

 Date: 12/02/2020 14:31:35
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for articles
-- ----------------------------
DROP TABLE IF EXISTS `articles`;
CREATE TABLE `articles`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime(0) NULL DEFAULT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of articles
-- ----------------------------
INSERT INTO `articles` VALUES (1, 'ajajajajaja', 'www.ggggg.com.br', 'jajajajaja', 1, '2020-02-07 18:32:10', '2020-02-07 18:32:10');
INSERT INTO `articles` VALUES (2, 'alakaka', 'www.awawa.com', 'wkwkwkww', 3, '2020-02-10 17:28:31', '2020-02-10 17:28:31');
INSERT INTO `articles` VALUES (3, 'alo', 'www.alo.com.br', 'alo', 9, '2020-02-11 18:01:44', '2020-02-11 18:01:44');
INSERT INTO `articles` VALUES (4, 'google', 'www.google.com', 'google', 12, '2020-02-11 18:18:42', '2020-02-11 18:18:42');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `created` datetime(0) NULL DEFAULT NULL,
  `modified` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (14, 'João Paulo Goulart Cavalheiro', 'headzao', 'headzaosmite@gmail.com', '$2y$10$Yw9c9lG6LM5KmkLo64Alu.buFj8Q6U.mgbnT8MiIgRnjnTgmCbA16', '2020-02-11 18:35:21', '2020-02-11 18:35:21');

SET FOREIGN_KEY_CHECKS = 1;
